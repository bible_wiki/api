<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return response("hi",403);
});

$router->get('test', function() use($router){
    $testvar = DB::connection('content')->select('SELECT * FROM category'); 
    return response()->json($testvar);
});

/* Everything for login, user profile, logout */
$router->group(['prefix'=>'auth'], function() use($router){
    /* Telegram Section */
    $router->group(['prefix'=>'tg'], function () use($router){
        $router->get('login', 'Auth\LoginController@tg');
    });
});
