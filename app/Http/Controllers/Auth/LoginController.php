<?php


namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;

class LoginController extends \App\Http\Controllers\Controller
{
    public function tg(Request $request){
        $id = $request->input("id");
        $first_name = $request->input("first_name");
        $last_name = $request->input("last_name");
        $username = $request->input("username");
        $photo_url = $request->input("photo_url");
        $auth_date = $request->input("auth_date");
        $hash = $request->input("hash");

        $token = "generated_token";
        return response("")
            ->header('Location',"https://portal.biblewiki.one")
            ->withCookie(new Cookie('api_token', $token,0,'/','.biblewiki.one'))
            ->withCookie(new Cookie('name', $username, 0, '/', '.biblewiki.one'));
    }
}
